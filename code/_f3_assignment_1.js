/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 */

define(['N/ui/serverWidget','N/search', 'N/record', 'N/currentRecord', 'N/task'], 
function(serverWidget, search, record, currentRecord, task) {
    function onRequest(context) {
        var form = serverWidget.createForm({
            title: 'Main Assignment'
        });
        form.addSubmitButton({
            label: 'Submit'
        });
        var filterGroup = form.addFieldGroup({
            id: 'filtergroup',
            label: 'Filters'
        });
       form.addField({
            id: 'todate',
            type: serverWidget.FieldType.DATE,
            label: 'To',
            container: 'filtergroup'
            });   
       form.addField({
            id: 'fromdate',
            type: serverWidget.FieldType.DATE,
            label: 'From',
            container: 'filtergroup'
            });    
          // add tab
          form.addTab({
            id: 'salesordertab',
            label: 'Sales Orders'
        });
        var sublist = form.addSublist({
            id: 'sosublist',
            type: serverWidget.SublistType.LIST, 
            label: 'Sales Orders',
            tab: 'salesordertab'
        });
        sublist.addField({
            id: 'internalid', 
            type: serverWidget.FieldType.TEXT,
            label: 'Internal Id'
        });
        sublist.addField({
            id: 'documentno', 
            type: serverWidget.FieldType.TEXT,
            label: 'Document Number'
        });
        sublist.addField({
            id: 'amount',
            type: serverWidget.FieldType.FLOAT,
            label: 'Amount'
        });
        sublist.addField({
            id: 'memo',
            type: serverWidget.FieldType.TEXT,
            label: 'Memo'
        });
        sublist.addField({
            id: 'checkbox',
            type: serverWidget.FieldType.CHECKBOX,
            label: 'Fulfill'
        });
        if(context.request.method === 'GET') {
           
         
      

        } else {
            // var cRecord = currentRecord.get();
            var sublistObject = form.getSublist({id: 'sosublist' });
            var sublistLineCount = context.request.getLineCount({group: 'sosublist'});
            var isSalesOrderSelected = false;
            log.debug('sublistLineCount', sublistLineCount);
            
            for(var line = 0; line < sublistLineCount; line++) {
               var checkboxValue =  context.request.getSublistValue({group: 'sosublist', name: 'checkbox', line: line});
               if(checkboxValue === 'T' || checkboxValue === 'True') {
                isSalesOrderSelected = true;
                var customRecord = record.create({
                    type: 'customrecord_f3_assignment_1_custrecord',
                    isDynamic: true
                });
                customRecord.setValue({
                    fieldId: 'custrecord_f3_internalid', 
                    value: context.request.getSublistValue({group: 'sosublist', name: 'internalid', line: line})
                });
                customRecord.setValue({
                    fieldId: 'custrecord_f3_processedcheckbox', 
                    value: false
                });
                customRecord.save({
                    ignoreMandatoryFields: true});
               } 
            }
        
            if(isSalesOrderSelected) {
                //  create a custom Record now and populate it with the sales order selected for fullfillment
                log.debug('isSalesOrderSelected', isSalesOrderSelected);
            var myTask = task.create({
                taskType : task.TaskType.SCHEDULED_SCRIPT,
                scriptId: 'customscript_f3_schedulescript',
                deploymentId: 'customdeploy_f3_schedulescript',
                });
// process 
                try {
                    var scheduledScriptTaskId = myTask.submit();
                    log.debug('schedule script running with id' + scheduledScriptTaskId);
                } catch (error) {
                    log.error(error.name);
                }
            }
            else {
                // populate the SO sublist
               
                var salesorderSearchObj = search.create({
                    type: "salesorder",
                    filters:
                    [
                        ["type","anyof","SalesOrd"], 
                        "AND", 
                        ["mainline","is","T"], 
                        "AND", 
                        ["status","anyof","SalesOrd:D","SalesOrd:B"], 
                        "AND", 
                        ["trandate","within",context.request.parameters.todate,context.request.parameters.fromdate]
                     ],
                    columns:
                    [
                    search.createColumn({
                        name: "ordertype",
                        sort: search.Sort.ASC,
                        label: "Order Type"
                    }),
                    search.createColumn({name: "mainline", label: "*"}),
                    search.createColumn({name: "trandate", label: "Date"}),
                    search.createColumn({name: "asofdate", label: "As-Of Date"}),
                    search.createColumn({name: "postingperiod", label: "Period"}),
                    search.createColumn({name: "taxperiod", label: "Tax Period"}),
                    search.createColumn({name: "type", label: "Type"}),
                    search.createColumn({name: "tranid", label: "Document Number"}),
                    search.createColumn({name: "entity", label: "Name"}),
                    search.createColumn({name: "account", label: "Account"}),
                    search.createColumn({name: "memo", label: "Memo"}),
                    search.createColumn({name: "amount", label: "Amount"}),
                    search.createColumn({name: "custbody_promisedate", label: "Promise Date"}),
                    search.createColumn({name: "custbody18", label: "Assy Dept"}),
                    search.createColumn({name: "custbody19", label: "Machine Shop"}),
                    search.createColumn({name: "custbody_cust_priority", label: "Priority"}),
                    search.createColumn({name: "custbody_pipeline", label: "HS PipeLine"}),
                    search.createColumn({name: "internalid", label: "Internal ID"}),
                    ]
                }).run();
              
                var results = salesorderSearchObj.getRange(0,1000);

                for (var i = 0; i < results.length; i++) {
                    sublistObject.setSublistValue({
                        id: 'internalid',
                        line: i,
                        value: results[i].getValue({name: 'internalid'})
                    });
                    sublistObject.setSublistValue({
                        id: 'documentno',
                        line: i,
                        value: results[i].getValue({name: 'tranid'})
                    });
                    
                    sublistObject.setSublistValue({
                        id: 'amount',
                        line: i,
                        value: results[i].getValue({name: 'amount'})
                    });
                    
                    sublistObject.setSublistValue({
                        id:'memo',
                        line: i,
                        value: results[i].getValue({name: 'memo'}) || " "
                    });
                    
                    sublistObject.setSublistValue({
                        id: 'checkbox',
                        line: i,
                        value: 'F'
                    });
    
                 }
            }
          
        }      
        context.response.writePage(form);

    }
    
    return {
        onRequest: onRequest
    };
});