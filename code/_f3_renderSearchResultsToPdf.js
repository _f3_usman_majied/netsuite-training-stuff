/**
* @NApiVersion 2.x
* @NScriptType Suitelet
*/

define(['N/search', 'N/render'], function(search, render){

    function onRequest(options) {
        var request = options.request;
        var response = options.response;

        var xmlStr = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n' +
            '<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n' +
            '<pdf lang="ru=RU" xml:lang="ru-RU"><head><link name="russianfont" type="font" subtype="opentype" src="NetSuiteFonts/verdana.ttf" src-bold="NetSuiteFonts/verdanab.ttf" src-italic="NetSuiteFonts/verdanai.ttf"  src-bolditalic="NetSuiteFonts/verdanabi.ttf" bytes="2"/></head><body font-family=\"russianfont\" font-size=\"18\">\n??????? ?????</body></pdf>';

        var rs = search.create({
            type: search.Type.TRANSACTION,
            columns: ['trandate', 'amount', 'entity'],
            filters: []
        }).run();

        var results = rs.getRange(0, 1000);
        var temp = "";
        var renderer = render.create();
        for (var i = 0; i < results.length; i++) {
                temp = temp + 'trandate: ' + results[i].getValue({name: 'trandate' }) +
                 'amount: '+ results[i].getValue({name: 'amount' }) + 
                 'entity: '+  results[i].getValue({name: 'entity' }) + '\n';
        }
        xmlStr = xmlStr.replace('??????? ?????', temp);
        renderer.templateContent = xmlStr;

        // renderer.addSearchResults({
        //     templateName: 'exampleName',
        //     searchResult: results
        // });

        var newfile = renderer.renderAsPdf();
        response.writeFile(newfile, false);
    }

    return {
        onRequest: onRequest
    };
});