/**
 *@NApiVersion 2.x
 *@NScriptType ClientScript
 */

define(['N/error'], function(error) {
    function pageInit(context) {
        if(context.mode !== 'create') return;
        log.debug({
            title: 'Success',
            details: 'Sales Order PageInit Called'
        });
        var currentRecord = context.currentRecord; 
        currentRecord.setValue({
            fieldId: 'entity',
            value: 9572 // set my company as default selected
        });
    }

    function saveRecord(context) {
        var currentRecord = context.currentRecord;
        console.log('currentRecord.getValue', currentRecord.getValue({ fieldId: 'entity'}),
         'currentRecord.getLineCount',currentRecord.getLineCount({sublistId: 'item'}))
        if(!currentRecord.getValue({ fieldId: 'entity'}) || 
        currentRecord.getLineCount({sublistId: 'item'}) < 1) {
            throw error.create({
                name: 'MISSING_REQ_ARG',
                message: 'Please enter all the necessary fields on the salesorder before saving'
            });
        }
        return true;
    }    

    function validateField(context) {
        var currentRecord = context.currentRecord;
        var sublistName = context.sublistId;
        var sublistFieldName = context.fieldId;
        console.log('sublistName',sublistName,'sublistFieldName',sublistFieldName);
        if(sublistName === 'item'){
           if(sublistFieldName === 'quantity'){
                if(currentRecord.getCurrentSublistValue({
               sublistId: sublistName,
               fieldId: sublistFieldName
            }) < 3)  {
            currentRecord.setValue({
                fieldId: 'memo',
                value: 'Quantity is less than 3',
                ignoreFieldChange: true
            })
        
        } else {
            currentRecord.setValue({
                fieldId: 'memo',
                value: 'Quantity accepted',
                ignoreFieldChange: true
            })
           
    }
        }
            }
        return true; 
    }

    function fieldChanged(context) {
        var currentRecord = context.currentRecord;
        var sublistName = context.sublistId;
        var sublistFieldName = context.fieldId;

        if(sublistName === 'item' && sublistFieldName === 'item') {
            currentRecord.setValue({
                fieldId: 'memo',
                value: 'item' + currentRecord.getCurrentSublistValue({
                    sublistId: 'item',
                    fieldId: 'item'
                }) + 'is selected'
            });
        }
    }

    function fieldChanged(context) {
        var currentRecord = context.currentRecord;
        var sublistName = context.sublistId;
        var fieldName = context.fieldId;
        console.log('sublistName',sublistName);
        if(fieldName === 'location' && !sublistName) {
            //    if(sublistName == 'item') {
            var locationFieldValue = currentRecord.getValue({
                fieldId: 'location'
            });
            var itemCount = currentRecord.getLineCount({
                sublistId: 'item'
            });
            console.log('itemCount',itemCount);
            if(itemCount > 0) {
                for(var lineNumber = 0; lineNumber < itemCount; lineNumber++) {
                    var selectedLine = currentRecord.selectLine({
                        sublistId: 'item',
                        line: lineNumber
                    });
                    selectedLine.setCurrentSublistValue({
                        sublistId: 'item',
                        fieldId: 'location',
                        value: locationFieldValue,
                        ignoreFieldChange: true
                    });
                    currentRecord.commitLine({sublistId: 'item'});
                }
            }
        //    }          
        }
    }

    function lineInit(context) {
        var currentRecord = context.currentRecord;
        var sublistName = context.sublistId;
        console.log("sublistName", sublistName);
        if(sublistName === 'item') {
            currentRecord.setCurrentSublistValue({
                sublistId: sublistName,
                fieldId: 'item',
                value: 55
            });
        }
    }
    function validateDelete(context) {
        var currentRecord = context.currentRecord;
        var sublistName = context.sublistId;

        if(sublistName === 'item' && currentRecord.getCurrentSublistValue({
            sublistId: sublistName,
            fieldId: 'partner'
            }) === '55') {
            currentRecord.setValue({
                fieldId: 'memo',
                value: 'Removing partner sublist'
                });
        }
        return true;
    }

    function validateInsert(context) {
        var currentRecord = context.currentRecord;
        var sublistName = context.sublistId;

        if(sublistName === 'partners' && currentRecord.getCurrentSublistValue({
            sublistId: sublistName,
            fieldId: 'partner'
            }) === '55') {
            currentRecord.setCurrentSublistValue({
                sublistId: sublistName,
                fieldId: 'contribution',
                value: '100.0%'
                });
        }
        console.log('contributions', currentRecord.getCurrentSublistValue({
            sublistId: sublistName,
            fieldId: 'contribution'
            }));
        return true;
    }

   return {
        pageInit: pageInit,
        fieldChanged: fieldChanged,
   } 
});