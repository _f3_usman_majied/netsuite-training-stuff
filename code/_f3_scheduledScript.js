/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 */

define(['N/search', 'N/record', 'N/email', 'N/runtime'], function(search, record, email, runtime) {

    function execute(context) {
        var customrecord_f3_assignment_1_custrecordSearchObj = search.create({
            type: "customrecord_f3_assignment_1_custrecord",
            filters:
            [
               ["custrecord_f3_processedcheckbox","is","F"]
            ],
            columns:
            [
               search.createColumn({
                  name: "name",
                  sort: search.Sort.ASC,
                  label: "Name"
               }),
               search.createColumn({name: "scriptid", label: "Script ID"}),
               search.createColumn({name: "custrecord_f3_processedcheckbox", label: "Processed"}),
               search.createColumn({name: "custrecord_f3_internalid", label: "Internal Id"})
            ]
         });
        //  var searchResultCount = customrecord_f3_assignment_1_custrecordSearchObj.runPaged().count;
        //  log.debug("customrecord_f3_assignment_1_custrecordSearchObj result count",searchResultCount);
         customrecord_f3_assignment_1_custrecordSearchObj.run().each(function(result){
            // .run().each has a limit of 4,000 results
            log.debug({
                details: 'Result ID:' + result.id
            });
            try {
               
                var fulfillmentRecord = record.transform({
                    fromType: record.Type.SALES_ORDER,
                    fromId: result.getValue({name: 'custrecord_f3_internalid'}),  
                    toType: record.Type.ITEM_FULFILLMENT,
                    defaultValues: {inventoryLocation: '1'},
                    isDynamic: true
                });
                var fulfillmentId = fulfillmentRecord.save({ ignoreMandatoryFields: true});
                log.debug({
                    details: 'fulfillmentId :' + fulfillmentId
                });
                record.submitFields({
                    type: 'customrecord_f3_assignment_1_custrecord',
                    id: result.id,
                    values: {'custrecord_f3_processedcheckbox': true}
                });
              
            } catch (error) {
                log.error('error: ', error.name);
            }

            return true;
         });
     
    }
    return {
        execute: execute
    };
});