/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 */

define(['N/currentRecord', 'N/url'], function(currentRecord, url){

    function tryThis(context){
        var record = currentRecord.get();
        var suiteUrl = url.resolveScript({
            scriptId: 'customscript_f3_eval_suitelet',
            deploymentId: 'customdeploy_f3_eval_deploy_suitelet',
// set the script Id and the deployment Id for the suitelet you want to pass the value to.           
            params : {
                customer_id : record.id, // pass customer id to the suitelet
            }
     });
        console.log('suiteUrl', suiteUrl);  
        window.open(suiteUrl, '_blank');
       
    }
    function pageInit(context) {
    } 

    return {
        pageInit: pageInit,
        tryThis: tryThis
    };
});
