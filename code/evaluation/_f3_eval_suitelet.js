/**
 * @NApiVersion 2.x
 * @NScriptType Suitelet
 */

 define(['N/ui/serverWidget','N/search', 'N/email'], 
 function(serverWidget, search ,email) {
     function onRequest(context) {
         var form = serverWidget.createForm({
             title: 'Pending Billing Sales Orders'
         });
         form.addSubmitButton({
             label: 'Submit'
         });
            // add tab
           form.addTab({
             id: 'salesordertab',
             label: 'Sales Orders'
         });
         var sublist = form.addSublist({
             id: 'sosublist',
             type: serverWidget.SublistType.LIST, 
             label: 'Sales Orders',
             tab: 'salesordertab'
         });
         sublist.addField({
             id: 'internalid', 
             type: serverWidget.FieldType.TEXT,
             label: 'Internal Id'
         });
         sublist.addField({
             id: 'documentno', 
             type: serverWidget.FieldType.TEXT,
             label: 'Document Number'
         });
         sublist.addField({
             id: 'amount',
             type: serverWidget.FieldType.FLOAT,
             label: 'Amount'
         });
         sublist.addField({
             id: 'email',
             type: serverWidget.FieldType.TEXT,
             label: 'email'
         });
         sublist.addField({
             id: 'checkbox',
             type: serverWidget.FieldType.CHECKBOX,
             label: 'Fulfill'
         });
         if(context.request.method === 'GET') {
            var customer_id = context.request.parameters.customer_id;
            
            // run a saved search

            var salesorderSearchObj = search.create({
                type: "salesorder",
                filters:
                [
                   ["type","anyof","SalesOrd"], 
                   "AND", 
                   ["mainline","is","T"], 
                   "AND", 
                   ["customermain.internalid","is",customer_id], 
                   "AND", 
                   ["status","anyof","SalesOrd:F"]
                ],
                columns:
                [
                   search.createColumn({
                      name: "ordertype",
                      sort: search.Sort.ASC,
                      label: "Order Type"
                   }),
                   search.createColumn({name: "mainline", label: "*"}),
                   search.createColumn({name: "trandate", label: "Date"}),
                   search.createColumn({name: "asofdate", label: "As-Of Date"}),
                   search.createColumn({name: "postingperiod", label: "Period"}),
                   search.createColumn({name: "taxperiod", label: "Tax Period"}),
                   search.createColumn({name: "type", label: "Type"}),
                   search.createColumn({name: "tranid", label: "Document Number"}),
                   search.createColumn({name: "entity", label: "Name"}),
                   search.createColumn({name: "account", label: "Account"}),
                   search.createColumn({name: "memo", label: "Memo"}),
                   search.createColumn({name: "amount", label: "Amount"}),
                   search.createColumn({name: "email", label: "Email"}),
                   search.createColumn({name: "internalid", label: "Internal ID"}),
                ]
             }).run();

             var results = salesorderSearchObj.getRange(0,1000);

             var sublistObject = form.getSublist({id: 'sosublist' });

             for (var i = 0; i < results.length; i++) {
                sublistObject.setSublistValue({
                    id: 'internalid',
                    line: i,
                    value: results[i].getValue({name: 'internalid'})
                });
                sublistObject.setSublistValue({
                    id: 'documentno',
                    line: i,
                    value: results[i].getValue({name: 'tranid'})
                });
                
                sublistObject.setSublistValue({
                    id: 'amount',
                    line: i,
                    value: results[i].getValue({name: 'amount'})
                });
                
                sublistObject.setSublistValue({
                    id:'email',
                    line: i,
                    value: results[i].getValue({name: 'email'}) || " "
                });
                
                sublistObject.setSublistValue({
                    id: 'checkbox',
                    line: i,
                    value: 'F'
                });

             }
 
         } else {
             // var cRecord = currentRecord.get();
             var sublistObject = form.getSublist({id: 'sosublist' });
             var sublistLineCount = context.request.getLineCount({group: 'sosublist'});
           
             if(sublistLineCount > 0) {
               
                 for(var line = 0; line < sublistLineCount; line++) {

                    // send separate emails for sales orders
                    email.send({
                        author: 1421,
                        recipients: ['usmanmajied@folio3.com'],
                        subject: 'This is a reminder email',
                        body: 'This is the final reminder for your sales order: ' +
                         context.request.getSublistValue({group: 'sosublist', name: 'internalid', line: line}) + 
                         'of amount : ' +  context.request.getSublistValue({group: 'sosublist', name: 'amount', line: line})
                    });               
                 }
             
             }
         
         }      
         context.response.writePage(form);
     }
     
     return {
         onRequest: onRequest
     };
 });