/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 */

define([], function(){
    function beforeLoad(context) {
        log.debug('before load called');
        if(context.type !== context.UserEventType.CREATE) return;
        
        context.form.addButton({
            id: "_f3_send_payment_reminder",
            label: "Send Payment Reminder",
            functionName: onButtonClicked(context)
        });
    }
   function onButtonClicked(context) {
        log.debug('context received', context);
    }
   
    return {
        beforeLoad: beforeLoad
    };

});