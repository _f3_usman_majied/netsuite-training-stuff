/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 */

define(['N/https', 'N/record', 'N/search'], function(https, record, search){

     function  getInputData() {
        // var response = https.get({
        //     url: 'https://0648c6f77e5cbdb0fd0bfff018c42578:9b497a1040bdc348b35bc40c22d1365a@new-folio3-test-store.myshopify.com/admin/orders.json'
        // });    
        //return JSON.parse(response.body).orders;
        
        return [ 
            {
                "id": 4118884286511,
                "admin_graphql_api_id": "gid://shopify/Order/4118884286511",
                "app_id": 1354745,
                "browser_ip": null,
                "buyer_accepts_marketing": false,
                "cancel_reason": null,
                "cancelled_at": null,
                "cart_token": null,
                "checkout_id": 23636928823343,
                "checkout_token": "eb3ddd04fb76cddc6373ff912ef29487",
                "client_details": {
                  "accept_language": null,
                  "browser_height": null,
                  "browser_ip": null,
                  "browser_width": null,
                  "session_hash": null,
                  "user_agent": null
                },
                "closed_at": null,
                "confirmed": true,
                "contact_email": "newcustomer@gmail.com",
                "created_at": "2021-08-30T10:39:25-04:00",
                "currency": "USD",
                "current_subtotal_price": "0.00",
                "current_subtotal_price_set": {
                  "shop_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  },
                  "presentment_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  }
                },
                "current_total_discounts": "0.00",
                "current_total_discounts_set": {
                  "shop_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  },
                  "presentment_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  }
                },
                "current_total_duties_set": null,
                "current_total_price": "0.00",
                "current_total_price_set": {
                  "shop_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  },
                  "presentment_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  }
                },
                "current_total_tax": "0.00",
                "current_total_tax_set": {
                  "shop_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  },
                  "presentment_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  }
                },
                "customer_locale": "en",
                "device_id": null,
                "discount_codes": [],
                "email": "newcustomer@gmail.com",
                "financial_status": "paid",
                "fulfillment_status": "fulfilled",
                "gateway": null,
                "landing_site": null,
                "landing_site_ref": null,
                "location_id": null,
                "name": "#6785",
                "note": null,
                "note_attributes": [],
                "number": 5785,
                "order_number": 6785,
                "order_status_url": "https://new-folio3-test-store.myshopify.com/10721612/orders/e69be7719e4bc928ad41aa9bd8351d0e/authenticate?key=6c35e685f76645f3354affd6aebe93fa",
                "original_total_duties_set": null,
                "payment_gateway_names": [],
                "phone": null,
                "presentment_currency": "USD",
                "processed_at": "2021-08-30T10:39:25-04:00",
                "processing_method": "manual",
                "reference": null,
                "referring_site": null,
                "source_identifier": null,
                "source_name": "shopify_draft_order",
                "source_url": null,
                "subtotal_price": "0.00",
                "subtotal_price_set": {
                  "shop_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  },
                  "presentment_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  }
                },
                "tags": "",
                "tax_lines": [],
                "taxes_included": true,
                "test": false,
                "token": "e69be7719e4bc928ad41aa9bd8351d0e",
                "total_discounts": "0.00",
                "total_discounts_set": {
                  "shop_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  },
                  "presentment_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  }
                },
                "total_line_items_price": "0.00",
                "total_line_items_price_set": {
                  "shop_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  },
                  "presentment_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  }
                },
                "total_outstanding": "0.00",
                "total_price": "0.00",
                "total_price_set": {
                  "shop_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  },
                  "presentment_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  }
                },
                "total_price_usd": "0.00",
                "total_shipping_price_set": {
                  "shop_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  },
                  "presentment_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  }
                },
                "total_tax": "0.00",
                "total_tax_set": {
                  "shop_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  },
                  "presentment_money": {
                    "amount": "0.00",
                    "currency_code": "USD"
                  }
                },
                "total_tip_received": "0.00",
                "total_weight": 0,
                "updated_at": "2021-09-13T10:42:15-04:00",
                "user_id": 48355649,
                "billing_address": {
                  "first_name": "New",
                  "address1": "testing address",
                  "phone": "+16465553890",
                  "city": "New York",
                  "zip": "10001",
                  "province": "New York",
                  "country": "United States",
                  "last_name": "Customer",
                  "address2": "testing address",
                  "company": "Customer Company",
                  "latitude": 40.7479014,
                  "longitude": -73.98800539999999,
                  "name": "New Customer",
                  "country_code": "US",
                  "province_code": "NY"
                },
                "customer": {
                  "id": 3516160901167,
                  "email": "newcustomer@gmail.com",
                  "accepts_marketing": false,
                  "created_at": "2021-01-17T05:30:28-05:00",
                  "updated_at": "2021-08-30T10:39:25-04:00",
                  "first_name": "New",
                  "last_name": "Customer",
                  "orders_count": 62,
                  "state": "disabled",
                  "total_spent": "6432.02",
                  "last_order_id": 4118884286511,
                  "note": "",
                  "verified_email": true,
                  "multipass_identifier": null,
                  "tax_exempt": false,
                  "phone": null,
                  "tags": "",
                  "last_order_name": "#6785",
                  "currency": "USD",
                  "accepts_marketing_updated_at": "2021-01-17T05:30:28-05:00",
                  "marketing_opt_in_level": null,
                  "tax_exemptions": [],
                  "admin_graphql_api_id": "gid://shopify/Customer/3516160901167",
                  "default_address": {
                    "id": 6207911133231,
                    "customer_id": 3516160901167,
                    "first_name": "New",
                    "last_name": "Customer",
                    "company": "Customer Company",
                    "address1": "testing address",
                    "address2": "testing address",
                    "city": "New York",
                    "province": "New York",
                    "country": "United States",
                    "zip": "10001",
                    "phone": "+16465553890",
                    "name": "New Customer",
                    "province_code": "NY",
                    "country_code": "US",
                    "country_name": "United States",
                    "default": true
                  }
                },
                "discount_applications": [],
                "fulfillments": [
                  {
                    "id": 3487852068911,
                    "admin_graphql_api_id": "gid://shopify/Fulfillment/3487852068911",
                    "created_at": "2021-09-13T04:26:03-04:00",
                    "location_id": 2866241,
                    "name": "#6785.1",
                    "order_id": 4118884286511,
                    "receipt": {},
                    "service": "manual",
                    "shipment_status": null,
                    "status": "success",
                    "tracking_company": "Amazon Logistics UK",
                    "tracking_number": "4123789",
                    "tracking_numbers": [
                      "4123789"
                    ],
                    "tracking_url": "https://www.amazon.co.uk/gp/help/customer/display.html?nodeId=201910530",
                    "tracking_urls": [
                      "https://www.amazon.co.uk/gp/help/customer/display.html?nodeId=201910530"
                    ],
                    "updated_at": "2021-09-13T04:26:03-04:00",
                    "line_items": [
                      {
                        "id": 10474470735919,
                        "admin_graphql_api_id": "gid://shopify/LineItem/10474470735919",
                        "destination_location": {
                          "id": 3019076468783,
                          "country_code": "US",
                          "province_code": "NY",
                          "name": "New Customer",
                          "address1": "testing address",
                          "address2": "testing address",
                          "city": "New York",
                          "zip": "10001"
                        },
                        "fulfillable_quantity": 0,
                        "fulfillment_service": "manual",
                        "fulfillment_status": "fulfilled",
                        "gift_card": false,
                        "grams": 0,
                        "name": "This is 28th feb item",
                        "origin_location": {
                          "id": 1682756534319,
                          "country_code": "CA",
                          "province_code": "ON",
                          "name": "New Folio3 Test Store ",
                          "address1": "600 Byron Street",
                          "address2": "",
                          "city": "S Whitby",
                          "zip": "L1N 4R5"
                        },
                        "price": "0.00",
                        "price_set": {
                          "shop_money": {
                            "amount": "0.00",
                            "currency_code": "USD"
                          },
                          "presentment_money": {
                            "amount": "0.00",
                            "currency_code": "USD"
                          }
                        },
                        "product_exists": true,
                        "product_id": 6536928788527,
                        "properties": [],
                        "quantity": 1,
                        "requires_shipping": true,
                        "sku": "28thFebitem",
                        "taxable": true,
                        "title": "This is 28th feb item",
                        "total_discount": "0.00",
                        "total_discount_set": {
                          "shop_money": {
                            "amount": "0.00",
                            "currency_code": "USD"
                          },
                          "presentment_money": {
                            "amount": "0.00",
                            "currency_code": "USD"
                          }
                        },
                        "variant_id": 39251617087535,
                        "variant_inventory_management": "shopify",
                        "variant_title": "",
                        "vendor": "Vendor",
                        "tax_lines": [],
                        "duties": [],
                        "discount_allocations": []
                      }
                    ]
                  }
                ],
                "line_items": [
                  {
                    "id": 10474470735919,
                    "admin_graphql_api_id": "gid://shopify/LineItem/10474470735919",
                    "destination_location": {
                      "id": 3019076468783,
                      "country_code": "US",
                      "province_code": "NY",
                      "name": "New Customer",
                      "address1": "testing address",
                      "address2": "testing address",
                      "city": "New York",
                      "zip": "10001"
                    },
                    "fulfillable_quantity": 0,
                    "fulfillment_service": "manual",
                    "fulfillment_status": "fulfilled",
                    "gift_card": false,
                    "grams": 0,
                    "name": "This is 28th feb item",
                    "origin_location": {
                      "id": 1682756534319,
                      "country_code": "CA",
                      "province_code": "ON",
                      "name": "New Folio3 Test Store ",
                      "address1": "600 Byron Street",
                      "address2": "",
                      "city": "S Whitby",
                      "zip": "L1N 4R5"
                    },
                    "price": "0.00",
                    "price_set": {
                      "shop_money": {
                        "amount": "0.00",
                        "currency_code": "USD"
                      },
                      "presentment_money": {
                        "amount": "0.00",
                        "currency_code": "USD"
                      }
                    },
                    "product_exists": true,
                    "product_id": 6536928788527,
                    "properties": [],
                    "quantity": 1,
                    "requires_shipping": true,
                    "sku": "28thFebitem",
                    "taxable": true,
                    "title": "This is 28th feb item",
                    "total_discount": "0.00",
                    "total_discount_set": {
                      "shop_money": {
                        "amount": "0.00",
                        "currency_code": "USD"
                      },
                      "presentment_money": {
                        "amount": "0.00",
                        "currency_code": "USD"
                      }
                    },
                    "variant_id": 39251617087535,
                    "variant_inventory_management": "shopify",
                    "variant_title": "",
                    "vendor": "Vendor",
                    "tax_lines": [],
                    "duties": [],
                    "discount_allocations": []
                  }
                ],
                "refunds": [
                  {
                    "id": 812331204655,
                    "admin_graphql_api_id": "gid://shopify/Refund/812331204655",
                    "created_at": "2021-09-13T10:42:15-04:00",
                    "note": null,
                    "order_id": 4118884286511,
                    "processed_at": "2021-09-13T10:42:15-04:00",
                    "restock": true,
                    "total_duties_set": {
                      "shop_money": {
                        "amount": "0.00",
                        "currency_code": "USD"
                      },
                      "presentment_money": {
                        "amount": "0.00",
                        "currency_code": "USD"
                      }
                    },
                    "user_id": 48355649,
                    "order_adjustments": [],
                    "transactions": [],
                    "refund_line_items": [
                      {
                        "id": 306012848175,
                        "line_item_id": 10474470735919,
                        "location_id": 2866241,
                        "quantity": 1,
                        "restock_type": "return",
                        "subtotal": 0,
                        "subtotal_set": {
                          "shop_money": {
                            "amount": "0.00",
                            "currency_code": "USD"
                          },
                          "presentment_money": {
                            "amount": "0.00",
                            "currency_code": "USD"
                          }
                        },
                        "total_tax": 0,
                        "total_tax_set": {
                          "shop_money": {
                            "amount": "0.00",
                            "currency_code": "USD"
                          },
                          "presentment_money": {
                            "amount": "0.00",
                            "currency_code": "USD"
                          }
                        },
                        "line_item": {
                          "id": 10474470735919,
                          "admin_graphql_api_id": "gid://shopify/LineItem/10474470735919",
                          "destination_location": {
                            "id": 3019076468783,
                            "country_code": "US",
                            "province_code": "NY",
                            "name": "New Customer",
                            "address1": "testing address",
                            "address2": "testing address",
                            "city": "New York",
                            "zip": "10001"
                          },
                          "fulfillable_quantity": 0,
                          "fulfillment_service": "manual",
                          "fulfillment_status": "fulfilled",
                          "gift_card": false,
                          "grams": 0,
                          "name": "This is 28th feb item",
                          "origin_location": {
                            "id": 1682756534319,
                            "country_code": "CA",
                            "province_code": "ON",
                            "name": "New Folio3 Test Store ",
                            "address1": "600 Byron Street",
                            "address2": "",
                            "city": "S Whitby",
                            "zip": "L1N 4R5"
                          },
                          "price": "0.00",
                          "price_set": {
                            "shop_money": {
                              "amount": "0.00",
                              "currency_code": "USD"
                            },
                            "presentment_money": {
                              "amount": "0.00",
                              "currency_code": "USD"
                            }
                          },
                          "product_exists": true,
                          "product_id": 6536928788527,
                          "properties": [],
                          "quantity": 1,
                          "requires_shipping": true,
                          "sku": "28thFebitem",
                          "taxable": true,
                          "title": "This is 28th feb item",
                          "total_discount": "0.00",
                          "total_discount_set": {
                            "shop_money": {
                              "amount": "0.00",
                              "currency_code": "USD"
                            },
                            "presentment_money": {
                              "amount": "0.00",
                              "currency_code": "USD"
                            }
                          },
                          "variant_id": 39251617087535,
                          "variant_inventory_management": "shopify",
                          "variant_title": "",
                          "vendor": "Vendor",
                          "tax_lines": [],
                          "duties": [],
                          "discount_allocations": []
                        }
                      }
                    ],
                    "duties": []
                  }
                ],
                "shipping_address": {
                  "first_name": "New",
                  "address1": "testing address",
                  "phone": "+16465553890",
                  "city": "New York",
                  "zip": "10001",
                  "province": "New York",
                  "country": "United States",
                  "last_name": "Customer",
                  "address2": "testing address",
                  "company": "Customer Company",
                  "latitude": 40.7479014,
                  "longitude": -73.98800539999999,
                  "name": "New Customer",
                  "country_code": "US",
                  "province_code": "NY"
                },
                "shipping_lines": []
              }
        ]
    }

    function map(context) {
        // context.body for getting response data, i.e. orders 
 
            var jsonParseContext = JSON.parse(context.value);
            var customer = search.create({
                type:  record.Type.CUSTOMER,
                filters: [ ["email","is", jsonParseContext.customer.email]],
                columns: ['internalid'],
                title: 'Customer Search'
                }).run();
                
              var customerResult = customer.getRange(0,50);
            log.debug('customerResult',customerResult);
        if(customerResult.length == 0) {
            try {
                var billing_address =  JSON.parse(context.value.billing_address);
                log.debug('jsonParseContext.billing_address.company', billing_address.company);
                var customerRecord = record.create({
                    type: record.Type.CUSTOMER,
                    isDynamic: true
                });
                customerRecord.setValue({
                    fieldId: 'autoname',
                    value: true
                });
                customerRecord.setValue({
                    fieldId: 'companyname',
                    value: billing_address.company
                });
                customerRecord.setValue({
                    fieldId: 'subsidiary',
                    value: '1'
                });
                customerRecord.setValue({
                    fieldId: 'email',
                    value: jsonParseContext.customer.email
                });
              var newCustomerId = customerRecord.save({ignoreMandatoryFields: true});
            } catch (error) {   
                log.error('customer record error', error);
            }
          
        }

        // create sales order
        var item = search.create({
            type: record.Type.INVENTORY_ITEM,
            filters: [ ["itemid","is", jsonParseContext.line_items[0].sku]],
            columns: ['internalid'],
            title: 'Item Search'
            }).run(); 
        
        var itemResult = item.getRange(0,50);
        
        log.debug('itemResult',itemResult)
      
        // set line items in items sublist
       if(itemResult.length > 0) {
        try {
            var salesOrder = record.create({
                type: record.Type.SALES_ORDER,
                isDynamic: true
            });
            salesOrder.setValue({
                fieldId: 'currency',
                value: '1'
            });
            salesOrder.setValue({
                fieldId: 'entity',
                value: newCustomerId || customerResult[0].id
            });
            salesOrder.setValue({
                fieldId: 'subsidiary',
                value: '1'
            });
            // for(var lineItem = 0; lineItem < jsonParseContext.line_items.length; lineItem++) {
            salesOrder.selectNewLine({
                sublistId: 'item'
            });
            // salesOrder.setCurrentSublistValue({
            //     sublistId: 'item',
            //     fieldId: 'item_display',
            //     value: jsonParseContext.line_items[0].name
            // });
            salesOrder.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'item',
                value: itemResult[0].id
            });
            salesOrder.setCurrentSublistValue({
                sublistId: 'item',
                fieldId: 'amount',
                value: 1234
            });
            // salesOrder.setCurrentSublistValue({
            //     sublistId: 'item',
            //     fieldId: 'price',
            //     value: jsonParseContext.line_items[0].price
            // });
            // salesOrder.setCurrentSublistValue({
            //     sublistId: 'item',
            //     fieldId: 'quantity',
            //     value: jsonParseContext.line_items[0].quantity
            // });
            salesOrder.commitLine({
                sublistId: 'item'
            });

            salesOrder.save({ignoreManditoryFields: true});
            log.debug('salesOrder created with id: ' +  salesOrderId);
        // }
            
        } catch (error) {
            log.error('Set item error',error);
        }
       }
    }

    return {
        getInputData: getInputData,
        map: map
    };

});