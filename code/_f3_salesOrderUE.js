/**
 *@NApiVersion 2.x
 *@NScriptType UserEventScript
 */
 define(['N/record'], function(record){

    function afterSubmit(context) {
        log.debug('After Submit called');
        // if(context.type !==  context.UserEventType.CREATE) return;
        var salesOrderRecord = context.newRecord;
        // check if payment method is set
        if(salesOrderRecord.getValue('paymentmethod')) {
            // orderstatus
            var cashSale = record.transform({
                fromType: record.Type.SALES_ORDER,
                fromId: salesOrderRecord.id,
                toType: record.Type.CASH_SALE,
                isDynamic: true
            });

            cashSale.setValue('entity', 9572);
            cashSale.setValue('currency', 1); // USD

            try {
                var cashSaleId = cashSale.save({
                    ignoreMandatoryFields: true
                });
                log.debug('Cash Sale created successfully', 'Id: ' + cashSaleId);
            } catch (error) {
                log.error(error.name);
            }
        } else {
            var invoice = record.transform({
                fromType: record.Type.SALES_ORDER,
                fromId: salesOrderRecord.id,
                toType: record.Type.INVOICE,
                isDynamic: true
            });
            log.debug('Else');

            invoice.setValue('currency', 1); // USD
            invoice.setValue('location', 2)  // Berlin
            invoice.setValue('tobeemailed', false);
            try {
                log.debug('Else', 'try');
                var invoiceId = invoice.save({
                    ignoreMandatoryFields: true
                });
                log.debug('Invoice created successfully', 'Id: ' + invoiceId);
            } catch (error) {
                log.error(error.name, error);
            }
        }
  
        var salesOrderStatus = salesOrderRecord.getValue('orderstatus');

        if(salesOrderStatus !== 'Fulfilled') {
            // create an item fulfillment
            log.debug('fulfillment');
            
            var itemFulfillment = record.transform({
                fromType: record.Type.SALES_ORDER,
                fromId: salesOrderRecord.id,
                toType: record.Type.ITEM_FULFILLMENT,
                isDynamic: true
            });
            itemFulfillment.setValue('memo', 'This is a fulfillment created from User Event');
            itemFulfillment.setValue('shipphone', '(444) 444-4444');

            try {
                var itemFulfillmentId = itemFulfillment.save({
                    ignoreMandatoryFields: true
                });
                log.debug('Item Fulfillment created successfully', 'Id: ' + itemFulfillmentId);
            } catch (error) {
                log.error(error.name);
            }
        }
    }

    return {
        afterSubmit: afterSubmit
    };
 });