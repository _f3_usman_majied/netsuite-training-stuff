/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 */

define(['N/currentRecord'], function(currentRecord){

    function tryThis(context){
      console.log('button clicked');     
    }
    function pageInit(context) {
      console.log('pageInit');     
    } 

    return {
        pageInit: pageInit,
        tryThis: tryThis
    };
});
